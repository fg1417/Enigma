This exercise asks you to implement an Enigma machine in C++. Enigma is the common name for the coding machine used by German forces in the Second World War. Two machines set up in the same way allowed the sending of a message securely between their users.

You will need to perform simple input/output operations to configure your Enigma machine from command line arguments and configuration files. Your Enigma machine should then encrypt (or decrypt) messages provided on the standard input stream, outputting the encrypted (or decrypted) message on the standard output stream.

You should implement your Enigma machine and its components in an object oriented manner using C++ classes.